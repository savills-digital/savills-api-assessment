import { Controller, Get, Query, Logger } from "@nestjs/common";
import { ExampleService } from "./example.service";
import { Foo, Bar, Baz } from "./types";

@Controller("example")
export class ExampleController {
  constructor(private exampleService: ExampleService, private logger: Logger) {}

  @Get("foo")
  async foo(): Promise<Foo> {
    return this.exampleService.foo();
  }

  @Get("bar")
  bar(@Query("baz") baz?: string): Bar | Baz {
    if (baz) {
      this.logger.log("baz not bar");
      return { baz };
    }

    return this.exampleService.bar();
  }
}
