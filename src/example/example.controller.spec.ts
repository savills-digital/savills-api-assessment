import { Test, TestingModule } from "@nestjs/testing";
import { ExampleController } from "./example.controller";
import { ExampleModule } from "./example.module";
import { ExampleService } from "./example.service";
import { Bar, Baz, Foo } from "./types";
import { Logger } from "@nestjs/common";

describe("Example Controller", () => {
  let controller: ExampleController;
  let exampleService: Partial<jest.Mocked<ExampleService>> = {};
  const logger: Partial<jest.Mocked<Logger>> = { log: jest.fn() };

  afterEach(() => {
    exampleService = {};
    logger.log?.mockReset();
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ExampleModule],
    })
      .overrideProvider(ExampleService)
      .useValue(exampleService)
      .overrideProvider(Logger)
      .useValue(logger)
      .compile();

    controller = module.get<ExampleController>(ExampleController);
  });

  it("foo resolves with ExampleService#foo", async () => {
    exampleService.foo = jest.fn();
    exampleService.foo.mockResolvedValue("test foo" as Foo);

    await expect(controller.foo()).resolves.toEqual("test foo");
  });

  it("bar returns ExampleService#bar", () => {
    exampleService.bar = jest.fn();
    exampleService.bar.mockReturnValue({ bar: "test bar" });

    expect(controller.bar()).toEqual<Bar>({ bar: "test bar" });
  });

  it.each(["bing", "bang", "boom"])(
    "bar returns Baz of %s when called with argument",
    (baz) => {
      expect(controller.bar(baz)).toEqual<Baz>({ baz });
      expect(logger.log).toBeCalledWith("baz not bar");
    }
  );
});
