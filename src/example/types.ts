export type Foo = "foo";
export type Bar = { bar: string };
export type Baz = { baz: string };
