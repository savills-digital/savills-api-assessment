import { Test, TestingModule } from "@nestjs/testing";
import { ExampleService } from "./example.service";
import { Foo, Bar } from "./types";

describe("ExampleService", () => {
  let service: ExampleService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ExampleService],
    }).compile();

    service = module.get<ExampleService>(ExampleService);
  });

  it("foo resolves with Foo", async () => {
    await expect(service.foo()).resolves.toEqual<Foo>("foo");
  });

  it("bar returns Bar", () => {
    expect(service.bar()).toEqual<Bar>({ bar: "bar" });
  });
});
