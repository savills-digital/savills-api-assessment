import { Module, Logger } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { ExampleModule } from "../example/example.module";

@Module({
  imports: [ConfigService, Logger, ExampleModule],
})
export class AppModule {}
