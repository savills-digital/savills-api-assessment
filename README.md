# savills-api-assessment

A TypeScript Node.js API.

## Assessment

### Endpoint

Implement an endpoint that returns a grid `n * n` of alternating characters `x` and `y`:

- `n` should default to 8
- `x` should default to `#`
- `y` should default to a single whitespace character
- point `0, 0` must be `x`
- grid rows must alternate ordering of the characters (see below)
- the response should be `text/plain`
- a default `n` row: `# # # # `
- a default `n` grid:

```text
# # # # 
 # # # #
# # # # 
 # # # #
# # # # 
 # # # #
# # # # 
 # # # #
```

- `n`, `x` and `y` should all be able to be provided through the request
- verb is your choice: provide a rationale in [documentation](documentation.md)
- how parameters are provided to the request is your choice: provide a rationale in [documentation](documentation.md)
- provide a full commit history:
  - keep the existing commit(s)
  - provide as zip or on a public repo (eg GitHub, Bitbucket etc)
- provide instructions in the [documentation](documentation.md) on how to query the endoint
- testing is important and so is coverage: give a brief rationale/explanation in the [documentation](documentation.md) of the level of coverage reached for the endpoint
- [check the project](#checking-the-project)

A basic example [module](src/example/example.module.ts), [service](src/example/example.service.ts) & [controller](src/example/example.controller.ts) is provided demonstrating usage, patterns & testing.

### Authentication

The following is theoretical and _should not_ need to be implemented. Think about how you might design a solution; we will discuss this in interview.

The API needs to be secured. Authentication will be handled by a third-party provider:

- on successful authentication, the provider redirects the client to a frontend application with a single use code in query parameter
- the single use code can be exchanged for an access token and a refresh token
- the access token is valid for 1 hour on login
- the refresh token is valid for 24 hours and can be exchanged for a new access token
- the provider has frontend & backend client libraries for:
  - exchanging the code for tokens
  - validating the access token
  - exchanging the refresh token for a new access token

Requirements:

- a user's session must only be valid for 15 minutes
- it is permissible for the session to be kept alive beyond the 15 minutes by requesting a refresh of expiry
- the API must use the access token to validate the user
- if the session has expired, the access & refresh tokens are no longer considered valid by the API

## Requirements

- Node.js

## Initial Setup

Run `npm install` to install dependencies.

## Development

This is a [NestJS](https://docs.nestjs.com/) app.

- `npm run start:dev` to run the application development mode
- `npm test` to test the project
- `npm run check-project` to [Check the project](#checking-the-project)

See output of `npm run` for all available commands.

## Checking the project

A command `npm run check-project` is provided. Imagine this command runs in CI infrastructure and PRs cannot be merged without it passing.

`check-project` does the following:

- Ensures the project compiles: `npm run build`
- Lints the project: `npm run lint`
- Runs unit tests: `npm run test:cov`
- Validates formatting: `npm run format:check`
