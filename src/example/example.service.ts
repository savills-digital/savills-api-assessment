import { Injectable } from "@nestjs/common";
import { Bar, Foo } from "./types";

@Injectable()
export class ExampleService {
  async foo(): Promise<Foo> {
    return "foo";
  }

  bar(): Bar {
    return { bar: "bar" };
  }
}
